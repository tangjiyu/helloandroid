import os
import sys

import shutil


class PostBuildHandler:
    """
    1. copy artifact apk file to artifacts folder
    2. git tag and push
    """

    def __init__(self, version_name, version_code):
        self.version_name = version_name
        self.version_code = version_code
        self.path_current = os.path.dirname(os.path.realpath(__file__))
        self.path_project_root = os.path.abspath(os.path.join(
            self.path_current, os.pardir))

    def dump(self):
        print 'pre_build: dump parameters'
        print '  version name: %s' % self.version_name
        print '  version code: %s' % self.version_code
        print '  project root: %s' % self.path_project_root
        print '  current     : %s' % self.path_current

    def copy_artifact_apk(self):
        print 'post_build: copy artifact apk'
        path_apk_src = os.path.join(
            self.path_current, 'build/outputs/apk/app-debug.apk')
        path_artifact_root = os.path.join(self.path_project_root, 'artifacts')
        path_apk_dest = os.path.join(path_artifact_root,
                                     'bibleverse-v%s_r%s_release.apk' % (
                                         self.version_name, self.version_code))
        if not os.path.exists(path_artifact_root):
            os.mkdir(path_artifact_root)
        if os.path.exists(path_apk_dest):
            os.remove(path_apk_dest)
        shutil.copyfile(path_apk_src, path_apk_dest)
        return True

    def git_tag(self):
        print 'post_build: git tag'
        self.__exec_cmd(
            'git tag %s_%s' % (self.version_name, self.version_code))
        return True

    def git_push(self):
        print 'post_build: git push'
        self.__exec_cmd('git push --follow-tags')
        self.__exec_cmd('git push --tags')
        return True

    @staticmethod
    def __exec_cmd(cmd):
        print 'exec: %s' % cmd
        os.system(cmd)


if __name__ == '__main__':
    if not sys.argv or len(sys.argv) != 3:
        print 'invalid arguments'
        sys.exit(1)

    version_name = sys.argv[1]
    version_code = sys.argv[2]

    handler = PostBuildHandler(version_name, version_code)
    handler.dump()

    if handler.copy_artifact_apk() is False:
        print 'copy apk failed'
        sys.exit(1)

    if handler.git_tag() is False:
        print 'git tag failed'
        sys.exit(1)

    if handler.git_push() is False:
        print 'git push failed'
        sys.exit(1)
