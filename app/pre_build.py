import os
import sys


class PreBuildHandler:
    """
    1. update build.gradle with version code and version name
    2. git commit with build.gradle change
    3. git pull
    """

    def __init__(self, version_name, version_code):
        self.version_name = version_name
        self.version_code = version_code
        self.path_current = os.path.dirname(os.path.realpath(__file__))
        self.path_project_root = os.path.abspath(os.path.join(
            self.path_current, os.pardir))

    def dump(self):
        print 'pre_build: dump parameters'
        print '  version name: %s' % self.version_name
        print '  version code: %s' % self.version_code
        print '  project root: %s' % self.path_project_root
        print '  current     : %s' % self.path_current

    def update_build_gradle(self):
        print 'pre_build: update build.gradle'
        path_build_gradle = os.path.join(self.path_current, 'build.gradle')
        path_build_gradle_new = os.path.join(self.path_current,
                                             'build.gradle.new')
        if not os.path.exists(path_build_gradle):
            return False
        if os.path.exists(path_build_gradle_new):
            os.remove(path_build_gradle_new)

        h = open(path_build_gradle, 'r')
        h_new = open(path_build_gradle_new, 'w')
        line = h.readline()
        while line:
            strip_line = line.strip(' ')
            strip_line = strip_line.strip('\t')
            if strip_line.startswith('versionCode '):
                strip_line = '        versionCode %s\n' % self.version_code
            elif strip_line.startswith('versionName '):
                strip_line = '        versionName "%s"\n' % self.version_name
            else:
                strip_line = line
            h_new.write(strip_line)
            line = h.readline()
        h.close()
        h_new.close()

        os.remove(path_build_gradle)
        os.rename(path_build_gradle_new, path_build_gradle)
        return True

    def git_commit(self):
        print 'pre_build: git commit'
        self.__exec_cmd('git add .')
        self.__exec_cmd('git commit -m "pre_%s_%s"' % (
            self.version_name, self.version_code))
        return True

    def git_pull(self):
        print 'pre_build: git pull'
        self.__exec_cmd('git pull')
        return True

    @staticmethod
    def __exec_cmd(cmd):
        print 'exec: %s' % cmd
        os.system(cmd)


if __name__ == '__main__':
    if not sys.argv or len(sys.argv) != 3:
        print 'invalid arguments'
        sys.exit(1)

    version_name = sys.argv[1]
    version_code = sys.argv[2]

    handler = PreBuildHandler(version_name, version_code)
    handler.dump()
    if handler.update_build_gradle() is False:
        print 'update build.gradle failed'
        sys.exit(1)

    if handler.git_commit() is False:
        print 'git commit failed'
        sys.exit(1)

    if handler.git_pull() is False:
        print 'git pull failed'
        sys.exit(1)
